import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class TesteAmazon {

private WebDriver driver;

@Before
public void abrir(){
    System.setProperty("webdriver.gecko.driver", "C:\\Windows\\geckodriver.exe");
    driver = new FirefoxDriver();
    driver.manage().window().maximize();
    driver.get("https://www.amazon.com.br");
    Assert.assertEquals("Amazon.com.br | Tudo pra você, de A a Z.", driver.getTitle());
    }
@After
public void sair(){
        driver.quit();
        }
@Test
    public void clicarVendaNaAmazon() throws InterruptedException {
    driver.findElement(By.xpath("//*[@id=\"nav-xshop\"]/a[1]")).click();
    Assert.assertEquals("Comece a vender na Amazon", driver.findElement(By.xpath("//*[@id=\"a-page\"]/div[2]/div/div/div/div/div/div[1]/div/div[5]/div/div/div[1]/div[2]/h1")).getText());
    }
@Test
    public void devePesquisarComBarraDePesquisa() {
    try {
        WebDriverWait driverWait = new WebDriverWait(driver, 2);
        driverWait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"twotabsearchtextbox\"]"))));
    } catch (Exception e) {
        Assert.fail("Tempo excedido para aguardar elemento");
    }
    driver.findElement(By.xpath("//*[@id=\"twotabsearchtextbox\"]")).sendKeys("ps5");
    driver.findElement(By.id("nav-search-submit-button")).click();
    Assert.assertTrue(driver.findElement(By.className("sg-col-inner")).getText().contains("resultados para"));
    }
@Test
    public void clicarMaisVendidosMenu()throws InterruptedException {
    Thread.sleep(3000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
    Thread.sleep(3000);
        WebDriverWait wait = new WebDriverWait(driver, 5);
    Thread.sleep(3000);
        wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Mais Vendidos")));
    }

@Test
    public void deveVerificarValoresDropdownPesquisa() {
        WebElement element = driver.findElement(By.id("searchDropdownBox"));
        Select box = new Select(element);
        //retorna uma lista de WebElements
        List<WebElement> options = box.getOptions();

        //Se uma determinada opção está presente no combo
        boolean encontrou = false;
        for (WebElement option : options) {
            if (option.getText().equals("Games")) {
                encontrou = true;
                break;
            }
        }
        Assert.assertTrue(encontrou);
    }


}